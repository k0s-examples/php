#!/bin/sh
docker run --rm --init -it \
  --user $(id -u) \
  --workdir /src \
  -p 3030:3000 \
  -v $(pwd):/src \
  -e COMMIT=$(git rev-parse --short=8 HEAD) \
  php:7.4-cli-alpine3.15 "$@"
 